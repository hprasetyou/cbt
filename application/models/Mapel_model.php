<?php
//application/models/Mapel_model.php
class Mapel_model extends CI_Model
{

	private $id;
	private $nama;
	private $jum_kd;


//set prop values

	public function init()
	{
		$this->id = uniqid();
		return $this;
	}

	public function set_id($val)
	{
		$this->id = $val;
		return $this;
	}

	public function set_nama($val)
	{
		$this->nama = $val;
		return $this;
	}

	public function set_jum_kd($val)
	{
		$this->jum_kd = $val;
		return $this;
	}


//get prop values

	public function get_id()
	{
		return $this->id;
	}

	public function get_nama()
	{
		return $this->nama;
	}

	public function get_jum_kd()
	{
		return $this->jum_kd;
	}

	public function create()
	{
		$this->db->insert("mapel",array(
			'id'=>$this->id,
			'nama'=>$this->nama,
			'jum_kd'=>$this->jum_kd,
			)
		);
	}


	public function get()
	{
		$q = $this->db->get("mapel");
		return $q->result();
	}


	public function update()
	{
		$this->db->where("id",$this->id);
		$this->db->update("mapel",array(
			'nama'=>$this->nama,
			'jum_kd'=>$this->jum_kd,
			)
		);
	}

	function trace_by_exam_code($kode_soal){
		$this->db->cache_on();
		$this->db->select('mapel.nama');
		$this->db->where('soal.kode_soal',$kode_soal);
		$this->db->from('mapel');
		$this->db->join('kd','kd.id_mapel=mapel.id');
		$this->db->join('bank_soal','bank_soal.id_kd=kd.id');
		$this->db->join('soal','bank_soal.id=soal.id_bank_soal');
		return $this->db->get()->row();
	}


	public function delete()
	{
		$this->db->where("id",$this->id);
		$this->db->delete("mapel");
	}


	//add your custom code here.....



}

 //end of file
 //application/model/mapel_model.php
