<?php
//application/models/Siswa_20162017_model.php
class Siswa_20162017_model extends CI_Model
{

	private $nis;
	private $nama;
	private $kode_kelas;
	private $img_url;
	private $password;
	private $status;


//set prop values



	public function set_nis($val)
	{
		$this->nis = $val;
		return $this;
	}

	public function set_nama($val)
	{
		$this->nama = $val;
		return $this;
	}

	public function set_kode_kelas($val)
	{
		$this->kode_kelas = $val;
		return $this;
	}

	public function set_password($val)
	{
		$this->password = $val;
		return $this;
	}

	public function set_status($val)
	{
		$this->status = $val;
		return $this;
	}

	public function set_img_url($val)
	{
		$this->img_url = $val;
		return $this;
	}


//get prop values

	public function get_nis()
	{
		return $this->nis;
	}

	public function get_nama()
	{
		return $this->nama;
	}

	public function get_kode_kelas()
	{
		return $this->kode_kelas;
	}

	public function get_password()
	{
		return $this->password;
	}

	public function get_status()
	{
		return $this->status;
	}

	public function create()
	{
		$this->db->insert("siswa_20162017",array(
			'nis'=>$this->nis,
			'nama'=>$this->nama,
			'kode_kelas'=>$this->kode_kelas,
			'img_url'=>$this->img_url,
			'password'=>$this->password,
			'status'=>$this->status,
			)
		);
	}


	public function get()
	{
		$q = $this->db->get("siswa_20162017");
		return $q->result();
	}


	public function show($id,$kolom = array('nis','password','nama','kode_kelas','status','img_url'))
	{

		$this->db->select($kolom);
		$this->db->where("nis",$id);
		$q = $this->db->get("siswa_20162017");
		$this->nis = $q->row()->nis;
		$this->nama = $q->row()->nama;
		$this->kode_kelas = $q->row()->kode_kelas;
		$this->img_url = $q->row()->img_url;
		$this->password = $q->row()->password;
		$this->status = $q->row()->status;

		return $q->row();
	}

	public function update()
	{
		$this->db->where("nis",$this->nis);
		$this->db->update("siswa_20162017",array(
			'nama'=>$this->nama,
			'kode_kelas'=>$this->kode_kelas,
			'password'=>$this->password,
			'img_url'=>$this->img_url,
			'status'=>$this->status,
			)
		);
	}


	public function delete()
	{
		$this->db->where("nis",$this->nis);
		$this->db->delete("siswa_20162017");
	}


	//add your custom code here.....



}

 //end of file
 //application/model/siswa_20162017_model.php
