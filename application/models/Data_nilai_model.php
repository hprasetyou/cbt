<?php
//application/models/Data_nilai_model.php
class Data_nilai_model extends CI_Model
{

	private $id;
	private $id_soal_uji;
	private $nis;
	private $benar;
	private $salah;
	private $nilai;
	private $mulai;
	private $selesai;
	private $ip;
	private $pelanggaran;
	private $ok;




//set prop values


	public function set_id($val)
	{
		$this->id = $val;
		return $this;
	}

	public function set_id_soal_uji($val)
	{
		$this->id_soal_uji = $val;
		return $this;
	}

	public function set_nis($val)
	{
		$this->nis = $val;
		return $this;
	}

	public function set_benar($val)
	{
		$this->benar = $val;
		return $this;
	}

	public function set_salah($val)
	{
		$this->salah = $val;
		return $this;
	}

	public function set_nilai($val)
	{
		$this->nilai = $val;
		return $this;
	}

	public function set_mulai($val)
	{
		$this->mulai = $val;
		return $this;
	}

	public function set_selesai($val)
	{
		$this->selesai = $val;
		return $this;
	}

	public function set_ip($val)
	{
		$this->ip = $val;
		return $this;
	}

		public function set_pelanggaran($val)
		{
			$this->pelanggaran = $val;
			return $this;
		}
			public function set_submit($val)
			{
				$this->ok = $val;
				return $this;
			}


//get prop values

	public function get_id()
	{
		return $this->id;
	}

	public function get_id_soal_uji()
	{
		return $this->id_soal_uji;
	}

	public function get_nis()
	{
		return $this->nis;
	}

	public function get_benar()
	{
		return $this->benar;
	}

	public function get_salah()
	{
		return $this->salah;
	}

	public function get_nilai()
	{
		return $this->nilai;
	}

	public function get_mulai()
	{
		return $this->mulai;
	}

	public function get_selesai()
	{
		return $this->selesai;
	}

	public function get_ip()
	{
		return $this->ip;
	}

	public function get_pelanggaran()
	{
		return $this->pelanggaran;
	}

	public function create()
	{
		if(!$this->data_nilai_exist()){
			$this->db->insert("data_nilai",array(
				'id_soal_uji'=>$this->id_soal_uji,
				'nis'=>$this->nis,
				'benar'=>$this->benar,
				'salah'=>$this->salah,
				'nilai'=>$this->nilai,
				'mulai'=>$this->mulai,
				'selesai'=>$this->selesai,
				'ip'=>$_SERVER['REMOTE_ADDR'],
				'pelanggaran'=>$this->pelanggaran,
				)
			);
		}

	}


	public function get()
	{
		$q = $this->db->get("data_nilai");
		return $q->result();
	}
	public function get_by_id($id,$nis,$kolom = array('id_soal_uji','nis','benar','salah','mulai','nilai','selesai','ip','pelanggaran'))
	{
			$this->db->cache_off();
			$this->db->select($kolom);
			$this->db->where('id',$id);
			$this->db->where('nis',$nis);
			$q = $this->db->get("data_nilai");
			return $q->row();
	}

	public function data_nilai_exist(){
		$this->db->where('id_soal_uji',$this->id_soal_uji);
		$this->db->where('nis',$this->nis);
		$q = $this->db->get("data_nilai");
		return $q->num_rows()>0?true:false;
	}

	public function show_by_nis_and_ujian($nis,$kode,$kolom = array(

		'data_nilai.id','id_soal_uji','nis','data_nilai.mulai','data_nilai.selesai','nilai','ip','pelanggaran','submit')){
			$this->db->cache_off();
		$this->db->select($kolom);
		$this->db->where('nis',$nis);
		$this->db->where('kode_soal',$kode);
		$this->db->from('data_nilai');
		$this->db->join('soal_uji','data_nilai.id_soal_uji=soal_uji.id');
		$result =  $this->db->get();
		return $result->num_rows()>0? $result->row():false;
	}


	public function list_by_nis($nis,$kolom = array('nis','data_nilai.mulai','data_nilai.selesai','nilai','submit')){
		$this->db->select($kolom);
		$this->db->where('nis',$nis);
		$this->db->where('submit','1');
		$this->db->from('data_nilai');
		$this->db->join('soal_uji','data_nilai.id_soal_uji=soal_uji.id');

		return $this->db->get()->result();
	}

	public function update()
	{
		$this->db->where("id",$this->id);
		$this->db->update("data_nilai",array(
			'id_soal_uji'=>$this->id_soal_uji,
			'nis'=>$this->nis,
			'benar'=>$this->benar,
			'salah'=>$this->salah,
			'nilai'=>$this->nilai,
			'mulai'=>$this->mulai,
			'selesai'=>$this->selesai,
			'ip'=> $_SERVER['REMOTE_ADDR'],
			'pelanggaran'=>$this->pelanggaran,
			'submit'=>$this->ok
			)
		);
	}


	public function delete()
	{
		$this->db->where("id",$this->id);
		$this->db->delete("data_nilai");
	}


	//add your custom code here.....



}

 //end of file
 //application/model/data_nilai_model.php
