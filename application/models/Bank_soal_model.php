<?php 
//application/models/Bank_soal_model.php 
class Bank_soal_model extends CI_Model 
{ 

	private $id;
	private $id_butir_soal;
	private $id_kd;


//set prop values

	public function init()
	{
		$this->id = uniqid();
		return $this;
	}

	public function set_id($val)
	{
		$this->id = $val;
		return $this;
	}

	public function set_id_butir_soal($val)
	{
		$this->id_butir_soal = $val;
		return $this;
	}

	public function set_id_kd($val)
	{
		$this->id_kd = $val;
		return $this;
	}


//get prop values

	public function get_id()
	{
		return $this->id;
	}

	public function get_id_butir_soal()
	{
		return $this->id_butir_soal;
	}

	public function get_id_kd()
	{
		return $this->id_kd;
	}

	public function create()
	{
		$this->db->insert("bank_soal",array(
			'id'=>$this->id,
			'id_butir_soal'=>$this->id_butir_soal,
			'id_kd'=>$this->id_kd,
			)
		);
	}


	public function get()
	{
		$q = $this->db->get("bank_soal");
		return $q->result();
	}


	public function update()
	{
		$this->db->where("id",$this->id);
		$this->db->update("bank_soal",array(
			'id_butir_soal'=>$this->id_butir_soal,
			'id_kd'=>$this->id_kd,
			)
		);
	}


	public function delete()
	{
		$this->db->where("id",$this->id);
		$this->db->delete("bank_soal");
	}


	//add your custom code here.....



}

 //end of file 
 //application/model/bank_soal_model.php