<?php
//application/models/Sh_guru_staff_model.php
class Sh_guru_staff_model extends CI_Model
{

	private $nip;
	private $id_gurusatff;
	private $nama_gurustaff;
	private $password;
	private $foto;
	private $alamat;
	private $email;
	private $telepon;


//set prop values

	public function init()
	{
		$this->nip = uniqid();
		return $this;
	}

	public function set_nip($val)
	{
		$this->nip = $val;
		return $this;
	}

	public function set_id_gurusatff($val)
	{
		$this->id_gurusatff = $val;
		return $this;
	}

	public function set_nama_gurustaff($val)
	{
		$this->nama_gurustaff = $val;
		return $this;
	}

	public function set_password($val)
	{
		$this->password = $val;
		return $this;
	}

	public function set_foto($val)
	{
		$this->foto = $val;
		return $this;
	}

	public function set_alamat($val)
	{
		$this->alamat = $val;
		return $this;
	}

	public function set_email($val)
	{
		$this->email = $val;
		return $this;
	}

	public function set_telepon($val)
	{
		$this->telepon = $val;
		return $this;
	}


//get prop values

	public function get_nip()
	{
		return $this->nip;
	}

	public function get_id_gurusatff()
	{
		return $this->id_gurusatff;
	}

	public function get_nama_gurustaff()
	{
		return $this->nama_gurustaff;
	}

	public function get_password()
	{
		return $this->password;
	}

	public function get_foto()
	{
		return $this->foto;
	}

	public function get_alamat()
	{
		return $this->alamat;
	}

	public function get_email()
	{
		return $this->email;
	}

	public function get_telepon()
	{
		return $this->telepon;
	}

	public function create()
	{
		$this->db->insert("sh_guru_staff",array(
			'nip'=>$this->nip,
			'id_gurusatff'=>$this->id_gurusatff,
			'nama_gurustaff'=>$this->nama_gurustaff,
			'password'=>$this->password,
			'foto'=>$this->foto,
			'alamat'=>$this->alamat,
			'email'=>$this->email,
			'telepon'=>$this->telepon,
			)
		);
	}


	public function get()
	{
		$q = $this->db->get("sh_guru_staff");
		return $q->result();
	}


	public function update()
	{
		$this->db->where("nip",$this->nip);
		$this->db->update("sh_guru_staff",array(
			'id_gurusatff'=>$this->id_gurusatff,
			'nama_gurustaff'=>$this->nama_gurustaff,
			'password'=>$this->password,
			'foto'=>$this->foto,
			'alamat'=>$this->alamat,
			'email'=>$this->email,
			'telepon'=>$this->telepon,
			)
		);
	}


	public function delete()
	{
		$this->db->where("nip",$this->nip);
		$this->db->delete("sh_guru_staff");
	}

	public function show($nip,$kolom= array('nip','nama_gurustaff')){

		$this->db->cache_on();
		$this->db->select($kolom);
		$this->db->from("sh_guru_staff");
		$this->db->where('nip',$nip);
		return $this->db->get()->row();
	}


	//add your custom code here.....



}

 //end of file
 //application/model/sh_guru_staff_model.php
