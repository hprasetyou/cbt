<?php
//application/models/Kd_model.php
class Kd_model extends CI_Model
{

	private $id;
	private $nama;
	private $semester;
	private $id_mapel;


//set prop values

	public function init()
	{
		$this->id = uniqid();
		return $this;
	}

	public function set_id($val)
	{
		$this->id = $val;
		return $this;
	}

	public function set_nama($val)
	{
		$this->nama = $val;
		return $this;
	}

	public function set_semester($val)
	{
		$this->semester = $val;
		return $this;
	}

	public function set_id_mapel($val)
	{
		$this->id_mapel = $val;
		return $this;
	}


//get prop values

	public function get_id()
	{
		return $this->id;
	}

	public function get_nama()
	{
		return $this->nama;
	}

	public function get_semester()
	{
		return $this->semester;
	}

	public function get_id_mapel()
	{
		return $this->id_mapel;
	}

	public function create()
	{
		$this->db->insert("kd",array(
			'id'=>$this->id,
			'nama'=>$this->nama,
			'semester'=>$this->semester,
			'id_mapel'=>$this->id_mapel,
			)
		);
	}


	public function get()
	{
		$q = $this->db->get("kd");
		return $q->result();
	}


	public function update()
	{
		$this->db->where("id",$this->id);
		$this->db->update("kd",array(
			'nama'=>$this->nama,
			'semester'=>$this->semester,
			'id_mapel'=>$this->id_mapel,
			)
		);
	}

	public function get_by_exam_code($kode_soal,$kolom=['distinct(kd.nama)']){
		$this->db->cache_on();
		$this->db->select($kolom,false);
		$this->db->from('kd');
		$this->db->join('bank_soal','bank_soal.id_kd=kd.id');
		$this->db->join('soal','soal.id_bank_soal=bank_soal.id');
		$this->db->where('kode_soal',$kode_soal);
		return $this->db->get()->result();
	}


	public function delete()
	{
		$this->db->where("id",$this->id);
		$this->db->delete("kd");
	}


	//add your custom code here.....



}

 //end of file
 //application/model/kd_model.php
