<?php
//application/models/Butir_soal_model.php
class Butir_soal_model extends CI_Model
{

	private $id;
	private $soal;
	private $opsi_a;
	private $opsi_b;
	private $opsi_c;
	private $opsi_d;
	private $opsi_e;
	private $kunci;


//set prop values

	public function init()
	{
		$this->id = uniqid();
		return $this;
	}

	public function set_id($val)
	{
		$this->id = $val;
		return $this;
	}

	public function set_soal($val)
	{
		$this->soal = $val;
		return $this;
	}

	public function set_opsi_a($val)
	{
		$this->opsi_a = $val;
		return $this;
	}

	public function set_opsi_b($val)
	{
		$this->opsi_b = $val;
		return $this;
	}

	public function set_opsi_c($val)
	{
		$this->opsi_c = $val;
		return $this;
	}

	public function set_opsi_d($val)
	{
		$this->opsi_d = $val;
		return $this;
	}

	public function set_opsi_e($val)
	{
		$this->opsi_e = $val;
		return $this;
	}

	public function set_kunci($val)
	{
		$this->kunci = $val;
		return $this;
	}


//get prop values

	public function get_id()
	{
		return $this->id;
	}

	public function get_soal()
	{
		return $this->soal;
	}

	public function get_opsi_a()
	{
		return $this->opsi_a;
	}

	public function get_opsi_b()
	{
		return $this->opsi_b;
	}

	public function get_opsi_c()
	{
		return $this->opsi_c;
	}

	public function get_opsi_d()
	{
		return $this->opsi_d;
	}

	public function get_opsi_e()
	{
		return $this->opsi_e;
	}

	public function get_kunci()
	{
		return $this->kunci;
	}

	public function create()
	{
		$this->db->insert("butir_soal",array(
			'id'=>$this->id,
			'soal'=>$this->soal,
			'opsi_a'=>$this->opsi_a,
			'opsi_b'=>$this->opsi_b,
			'opsi_c'=>$this->opsi_c,
			'opsi_d'=>$this->opsi_d,
			'opsi_e'=>$this->opsi_e,
			'kunci'=>$this->kunci,
			)
		);
	}


	public function show($id,$kolom=array('butir_soal.id','soal','opsi_a','opsi_b','opsi_c','opsi_d','opsi_e','kd.nama as kd','mapel.nama as mapel','kunci','is_essay'))
	{
		$this->db->cache_on();
		$this->db->select($kolom);
		$this->db->join('bank_soal','butir_soal.id=bank_soal.id_butir_soal');
		$this->db->join('kd','bank_soal.id_kd=kd.id');
		$this->db->join('mapel','kd.id_mapel=mapel.id');
		$q = $this->db->get_where("butir_soal",array("butir_soal.id"=>$id));
		return $q->row();
	}

	public function get_in($lst_id,$kolom=array('butir_soal.id','soal','opsi_a','opsi_b','opsi_c','opsi_d','opsi_e','kd.nama as kd','mapel.nama as mapel'))
	{

		$this->db->select($kolom);
		$this->db->where_in('butir_soal.id',$lst_id);
		$this->db->order_by('FIELD(butir_soal.id,'.implode(",", $lst_id).')', 'ASC',false);
		$this->db->from("butir_soal");
		$this->db->join('bank_soal','butir_soal.id=bank_soal.id_butir_soal');
		$this->db->join('kd','bank_soal.id_kd=kd.id');
		$this->db->join('mapel','kd.id_mapel=mapel.id');
		$q = $this->db->get();
		return $q->result();
	}


	public function update()
	{
		$this->db->where("id",$this->id);
		$this->db->update("butir_soal",array(
			'soal'=>$this->soal,
			'opsi_a'=>$this->opsi_a,
			'opsi_b'=>$this->opsi_b,
			'opsi_c'=>$this->opsi_c,
			'opsi_d'=>$this->opsi_d,
			'opsi_e'=>$this->opsi_e,
			'kunci'=>$this->kunci,
			)
		);
	}


	public function delete()
	{
		$this->db->where("id",$this->id);
		$this->db->delete("butir_soal");
	}

	public function get_by_kode_soal($kode_soal){
		$this->db->select();
		$this->db->from('butir_soal');
		$this->db->join('bank_soal','bank_soal.id_butir_soal=butir_soal.id');
		$this->db->join('soal','bank_soal.id=soal.id_bank_soal');
		$this->db->where("kode_soal",$kode_soal);
		$q = $this->db->get();
		return $q->result();
	}


	//add your custom code here.....



}

 //end of file
 //application/model/butir_soal_model.php
