<?php
//application/models/Soal_uji_model.php
class Soal_uji_model extends CI_Model
{

	private $id;
	private $kode_soal;
	private $nip;
	private $jenis_ujian;
	private $tingkat;
	private $ket;
	private $mulai;
	private $selesai;
	private $jum_soal;
	private $lama;
	private $passcode;
	private $status;
	private $acak;
	private $tampil_nilai;


//set prop values

	public function init()
	{
		$this->id = uniqid();
		return $this;
	}

	public function set_id($val)
	{
		$this->id = $val;
		return $this;
	}

	public function set_kode_soal($val)
	{
		$this->kode_soal = $val;
		return $this;
	}

	public function set_nip($val)
	{
		$this->nip = $val;
		return $this;
	}

	public function set_jenis_ujian($val)
	{
		$this->jenis_ujian = $val;
		return $this;
	}

	public function set_tingkat($val)
	{
		$this->tingkat = $val;
		return $this;
	}

	public function set_ket($val)
	{
		$this->ket = $val;
		return $this;
	}

	public function set_mulai($val)
	{
		$this->mulai = $val;
		return $this;
	}

	public function set_selesai($val)
	{
		$this->selesai = $val;
		return $this;
	}

	public function set_jum_soal($val)
	{
		$this->jum_soal = $val;
		return $this;
	}

	public function set_lama($val)
	{
		$this->lama = $val;
		return $this;
	}

	public function set_passcode($val)
	{
		$this->passcode = $val;
		return $this;
	}

	public function set_status($val)
	{
		$this->status = $val;
		return $this;
	}

	public function set_acak($val)
	{
		$this->acak = $val;
		return $this;
	}

	public function set_tampil_nilai($val)
	{
		$this->tampil_nilai = $val;
		return $this;
	}


//get prop values

	public function get_id()
	{
		return $this->id;
	}

	public function get_kode_soal()
	{
		return $this->kode_soal;
	}

	public function get_nip()
	{
		return $this->nip;
	}

	public function get_jenis_ujian()
	{
		return $this->jenis_ujian;
	}

	public function get_tingkat()
	{
		return $this->tingkat;
	}

	public function get_ket()
	{
		return $this->ket;
	}

	public function get_mulai()
	{
		return $this->mulai;
	}

	public function get_selesai()
	{
		return $this->selesai;
	}

	public function get_jum_soal()
	{
		return $this->jum_soal;
	}

	public function get_lama()
	{
		return $this->lama;
	}

	public function get_passcode()
	{
		return $this->passcode;
	}

	public function get_status()
	{
		return $this->status;
	}

	public function get_acak()
	{
		return $this->acak;
	}

	public function get_tampil_nilai()
	{
		return $this->tampil_nilai;
	}

	public function create()
	{
		$this->db->insert("soal_uji",array(
			'id'=>$this->id,
			'kode_soal'=>$this->kode_soal,
			'nip'=>$this->nip,
			'jenis_ujian'=>$this->jenis_ujian,
			'tingkat'=>$this->tingkat,
			'ket'=>$this->ket,
			'mulai'=>$this->mulai,
			'selesai'=>$this->selesai,
			'jum_soal'=>$this->jum_soal,
			'lama'=>$this->lama,
			'passcode'=>$this->passcode,
			'status'=>$this->status,
			'acak'=>$this->acak,
			'tampil_nilai'=>$this->tampil_nilai,
			)
		);
	}


	public function get()
	{
		$q = $this->db->get("soal_uji");
		return $q->result();
	}

	public function get_by_id($id,$kolom='*')
	{
		$this->db->cache_on();
		$this->db->select($kolom);
		$this->db->where('id',$id);
		$q = $this->db->get("soal_uji");
		return $q->row();
	}

	public function detail($kode_soal,$kolom="*")
	{
		//set cache on
		$this->db->cache_on();

		$this->db->select($kolom);
		$q = $this->db->get_where("soal_uji",array("kode_soal"=>$kode_soal));
		return $q->row();
	}

	public function get_active($except,$kolom = ['soal_uji.id','lama','jum_soal','kode_soal','nip','soal_uji.mulai','soal_uji.selesai']){
		$this->db->cache_on();
		$this->db->select($kolom);
		$this->db->where('status','1');
		if(count($except)>0)
		{
			$this->db->where_not_in('id',$except);
		}
		$this->db->from('soal_uji');

		return $this->db->get()->result();

	}




	public function update()
	{
		$this->db->where("id",$this->id);
		$this->db->update("soal_uji",array(
			'kode_soal'=>$this->kode_soal,
			'nip'=>$this->nip,
			'jenis_ujian'=>$this->jenis_ujian,
			'tingkat'=>$this->tingkat,
			'ket'=>$this->ket,
			'mulai'=>$this->mulai,
			'selesai'=>$this->selesai,
			'jum_soal'=>$this->jum_soal,
			'lama'=>$this->lama,
			'passcode'=>$this->passcode,
			'status'=>$this->status,
			'acak'=>$this->acak,
			'tampil_nilai'=>$this->tampil_nilai,
			)
		);
	}


	public function delete()
	{
		$this->db->where("id",$this->id);
		$this->db->delete("soal_uji");
	}


	//add your custom code here.....

	public function get_by_kode_soal($kode_soal){
		$q = $this->db->get_where("soal_uji",array("kode_soal"=>$kode_soal));
		return $q->row();
	}



}

 //end of file
 //application/model/soal_uji_model.php
