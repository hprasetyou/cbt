<?php

use \Firebase\JWT\JWT;
class Answer_model extends CI_Model{

  private $key = 'hahahaha';

  private $conn = 'http://localhost/ans_service/index.php/';

  private $nis;

  private $exam_code;

  private $req_body;

  public function set_nis($val){
    $this->nis = $val;
    return $this;
  }

  public function set_exam_code($val){
    $this->exam_code = $val;
    return $this;
  }

  public function set_req_body($val){
    $this->req_body = $val;
    return $this;
  }


  public function read($exam_code,$nis){
      $dir = "./upload/assignment/".$exam_code;
      $file_path = $dir ."/file_".$nis.".json";

        //ambil rencana nomor dan acakan
      $string = file_get_contents($file_path);
      return json_decode($string);
  }

  public function write($kode_soal,$nis,$input){
    //check if dir exist
    $dir = "./upload/assignment/".$kode_soal;
    $file_path = $dir ."/file_".$nis.".json";

    if (!is_dir($dir)) {
        mkdir($dir,0777);
    }
    if(!file_exists($file_path)){
      $user_data= json_encode($input);
      $file = fopen($file_path,"w");
      fwrite($file,$user_data);
      fclose($file);
    }else{
      $string = json_decode(file_get_contents($file_path));
    }
  }

  public function rewrite($exam_code,$nis,$input){
    $dir = "./upload/assignment/".$exam_code;
    $file_path = $dir ."/file_".$nis.".json";
    $user_data= json_encode($input);
    $file = fopen($file_path,"w");
    fwrite($file,$user_data);
    fclose($file);
  }

  public function test(){

    $token = $this->encode(array('nis'=>$this->nis,'exam_code'=>$this->exam_code));

    $ch = curl_init();

    // Now set some options (most are optional)

    // Set URL to download
    curl_setopt($ch, CURLOPT_URL, $this->conn.'test');

    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'token: '.$token
        ));
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);

    // Download the given URL, and return output
    $output = curl_exec($ch);

    // Close the cURL resource, and free system resources
    curl_close($ch);

    return $output;
  }


  public function read_on_remote(){
    $ch = curl_init();

    $token = $this->encode(array('nis'=>$this->nis,'exam_code'=>$this->exam_code));

    // Now set some options (most are optional)

    // Set URL to download
    curl_setopt($ch, CURLOPT_URL, $this->conn.'assignment');

    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'token: '.$token
        ));
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);

    // Download the given URL, and return output
    $output = curl_exec($ch);

    // Close the cURL resource, and free system resources
    curl_close($ch);

    return json_decode($output);
  }

  public function write_on_remote(){
    $ch = curl_init();

    $token = $this->encode(array('nis'=>$this->nis,'exam_code'=>$this->exam_code));

    // Now set some options (most are optional)

    // Set URL
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

    curl_setopt($ch, CURLOPT_URL, $this->conn.'assignment');

    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'token: '.$token
        ));
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_POSTFIELDS,$this->req_body);

    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);

    // Download the given URL, and return output
    $output = curl_exec($ch);

    // Close the cURL resource, and free system resources
    curl_close($ch);

    return json_decode($output);
  }

  public function rewrite_on_remote(){
    $ch = curl_init();

    $token = $this->encode(array('nis'=>$this->nis,'exam_code'=>$this->exam_code));

    // Now set some options (most are optional)

    // Set URL
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

    curl_setopt($ch, CURLOPT_URL, $this->conn.'assignment');

    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'token: '.$token
        ));
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_POSTFIELDS,$this->req_body);

    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);

    // Download the given URL, and return output
    $output = curl_exec($ch);

    // Close the cURL resource, and free system resources
    curl_close($ch);

    return json_decode($output);
  }



  private function encode($data){
    $data['iat'] = time();
    $data['exp'] = time()+60;
    $jwt = JWT::encode($data, $this->key);
    return $jwt;
  }

}
