<?php
//application/models/Soal_model.php
class Soal_model extends CI_Model
{

	private $id;
	private $id_bank_soal;
	private $kode_soal;


//set prop values


	public function set_id($val)
	{
		$this->id = $val;
		return $this;
	}

	public function set_id_bank_soal($val)
	{
		$this->id_bank_soal = $val;
		return $this;
	}


//get prop values

	public function get_id()
	{
		return $this->id;
	}

	public function get_id_bank_soal()
	{
		return $this->id_bank_soal;
	}

		public function get_kode_soal()
		{
			return $this->kode_soal;
		}
	public function create()
	{
		$this->db->insert("soal",array(
			'id'=>$this->id,
			'id_bank_soal'=>$this->id_bank_soal,
			'kode_soal'=>$this->kode_soal,
			)
		);
	}


	public function get()
	{
		$q = $this->db->get("soal");
		return $q->result();
	}


	public function update()
	{
		$this->db->where("id",$this->id);
		$this->db->update("soal",array(
			'id_bank_soal'=>$this->id_bank_soal,
			)
		);
	}


	public function delete()
	{
		$this->db->where("id",$this->id);
		$this->db->delete("soal");
	}


	//add your custom code here.....

	public function get_butir_soal_by_kode_soal($kd_soal,$kolom = array('soal.id','soal','opsi_a','opsi_b','opsi_c','opsi_d','opsi_e')){
		$this->db->where('kode_soal',$kd_soal);
		$this->db->select($kolom);
		$this->db->from('soal');
		$this->db->join('bank_soal', ' soal.id_bank_soal = bank_soal.id');
		$this->db->join('butir_soal','butir_soal.id = bank_soal.id_butir_soal');

		return $this->db->get()->result();
	}

}

 //end of file
 //application/model/soal_model.php
