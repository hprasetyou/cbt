<?php

class Question extends CI_Controller{
  public function __construct()
      {
        parent::__construct();
        $this->load->model(array('answer_model','soal_uji_model','soal_model','data_nilai_model','butir_soal_model','sh_guru_staff_model'));
        $userdata = auth_data();
        $this->nis = $userdata->user_id;
    //    $this->output->enable_profiler(TRUE);
      }
  //assign with session_id
  private $nis;

  public function index(){

  }

  public function show($exam_code){
    $this->load->library('numbertime');
    $detail = $this->data_nilai_model->show_by_nis_and_ujian($this->nis,$exam_code,array('data_nilai.mulai','data_nilai.selesai','jum_soal','submit','nip'));
    if($this->session->tempdata('ujian')==$exam_code){
      if($detail){
        $detail->guru = $this->sh_guru_staff_model->show($detail->nip);

        $selesai = new DateTime($detail->selesai);
        $now = new DateTime($this->numbertime->timestamp_to_time(time()));
        //
        if($now>$selesai){
          redirect('exam/finish/'.$exam_code);
        }else{

          $this->template->load('layout/main','question/show',array('exam_code'=>$exam_code,'detail'=>$detail));

        }
      }else{
        redirect('/');
      }
    }else{
      redirect('exam/show/'.$exam_code);
    }


  }

  public function take($exam_code,$req_number){
    //ambil soal dari server json file
    $data_soal = $this->soal_uji_model->get_by_kode_soal($exam_code);

    $this->answer_model
    ->set_nis($this->nis)
    ->set_exam_code($exam_code);
    $blueprint = $this->answer_model->read_on_remote();

    $start = floor($req_number/10)*10;
    $end = $start+10;
    $lst_id_soal = array();
    $lst_soal = array();


    foreach ($blueprint->data as $key => $value) {
      # code...
      if($key>=$start && $key<$end){
        $soal = $this->butir_soal_model
        ->show($value->id,['butir_soal.id','soal','opsi_a','opsi_b','opsi_c','opsi_d','opsi_e','kd.nama as kd','mapel.nama as mapel','sound','is_essay']);
        $soal->no = $value->no;
        $soal->opt_order = $value->opsi;
        array_push($lst_soal,$soal);
      }
    }
    print_json($lst_soal);

  }






}
