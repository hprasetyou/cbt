<?php

class Exam extends CI_Controller{
  public function __construct()
      {
        parent::__construct();
        $this->load->model(array('butir_soal_model','answer_model','mapel_model','kd_model','sh_guru_staff_model','soal_uji_model','soal_model','data_nilai_model'));
        $userdata = auth_data();
        $this->nis = $userdata->user_id;
        $this->load->library('numbertime');
  //      $this->output->enable_profiler(TRUE);
      }
  //assign with session_id
  private $nis;


  public function index(){
    //list completed exam
    $lst_hasil = $this->data_nilai_model->list_by_nis($this->nis,['id_soal_uji']);
    $lst_id = [];
    foreach ($lst_hasil as $key => $value) {
      # code...
      array_push($lst_id,$value->id_soal_uji);
    }
    //list of exam
    $exam_list = $this->soal_uji_model->get_active($lst_id);
    foreach ($exam_list as $key => $value) {
      # code...
      $value->guru = $this->sh_guru_staff_model->show($value->nip);
      $data_mapel = $this->mapel_model->trace_by_exam_code($value->kode_soal);
      $value->mapel = $data_mapel->nama;
    }
    //print_r($exam_list);
    $this->template->load('layout/main','exam/list',array('exam_list'=>$exam_list));
  }

  public function show($id){
    $this->load->helper('form');

    //get detail soal by id
    $detail = $this->soal_uji_model->detail($id);
    $detail->guru = $this->sh_guru_staff_model->show($detail->nip);
    $detail->mapel = $this->mapel_model->trace_by_exam_code($detail->kode_soal);
    $detail->kd = $this->kd_model->get_by_exam_code($detail->kode_soal);


    $alert = array();

    $selesai = new DateTime($detail->selesai);
    $mulai = new DateTime($detail->mulai);
    $now = new DateTime($this->numbertime->timestamp_to_time(time()));

    $data_pengerjaan = $this->data_nilai_model->show_by_nis_and_ujian($this->nis,$detail->kode_soal);
    $status_pengerjaan = array();
    if($data_pengerjaan){
    $status_pengerjaan['dikerjakan']=1;
    $status_pengerjaan['id']=$data_pengerjaan->id;
      if($data_pengerjaan->submit>0){
        //selesai

        $newalert = new \stdClass();
        $newalert->type='success';
        $newalert->msg='  Anda telah mengerjakan ujian ini, berikut '.
        anchor('exam/result_detail/'.$status_pengerjaan['id'],'Detail pengerjaan',array('style'=>'color:#444499'));
        array_push($alert,$newalert);
      }else{
        //masih berlangsung
        $newalert = new \stdClass();
        $newalert->type='info';
        $newalert->msg='Klik <a href="#" data-toggle="modal" data-target="#soalModal">Disini</a> untuk melanjutkan ujian ';
        array_push($alert,$newalert);
      }

    }else{
      //belum
      $status_pengerjaan['dikerjakan']=0;
      $newalert = new \stdClass();
      $newalert->type='info';
      $newalert->msg='Anda belum mengerjakan ujian ini ';
      array_push($alert,$newalert);

    }

    $status=array();
    if($now<$mulai){
      $status['status']='warning';
      $newalert = new \stdClass();
      $newalert->type='warning';
      $newalert->msg='Sekarang belum saatnya mengerjakan ujian ini';
      array_push($alert,$newalert);
    }else if($now>$selesai){
      $status['status']='warning';
      $newalert = new \stdClass();
      $newalert->type='danger';
      $newalert->msg='Ujian ini sudah expired, tidak dapat dikerjakan lagi';
      array_push($alert,$newalert);
    }else{
      $status['status']='info';
    }

    //load view
    $this->template->load('layout/main','exam/show',array(
      'detail'=>$detail,
      'status'=>$status,
      'alert'=>$alert,
      'data_pengerjaan'=>$data_pengerjaan,
      'status_pengerjaan'=>$status_pengerjaan
    ));
  //print_r($detail);
    //detail of exam, user can perform action to take the action here too
  }

  public function take(){
    //avoid undefined bla bla bla
    if($this->input->post('exam_code')){
      $data_soal = $this->soal_uji_model->get_by_kode_soal($this->input->post('exam_code'));
      $mulai = new DateTime($data_soal->mulai);
      $selesai = new DateTime($data_soal->selesai);
      $now = new DateTime($this->numbertime->timestamp_to_time(time()));

      if($now>$mulai && $now<$selesai){
        //must be post data, here is the logic to retreive exam password that being submit by user
        if(password_verify($this->input->post('exam_password'),$data_soal->passcode)){
            $this->session->set_tempdata('ujian', $this->input->post('exam_code'), 10);
            //save start time of user do the quiz
            $this->start_the_exam($data_soal->id,$data_soal->lama);
            //read all questionid member of soal_uji
            $list_id_soal = $this->soal_model->get_butir_soal_by_kode_soal($data_soal->kode_soal,array('butir_soal.id'));

            //create json file to hold ordering number of questionid and answer
            if($data_soal->acak==1){
              //if acak, make sure that ordering number of question is random
              shuffle($list_id_soal);
            }
            $i=0;

            foreach ($list_id_soal as $data) {
              # code...
              $i++;
              $data->no = $i;
              $data->ans = "";
              $data->opsi=($data_soal->acak==1)?str_shuffle('abcde'):'abcde';
            }
            //save json file
            $output = array('time'=>0,'pelanggaran'=>0,'data'=>$list_id_soal);
            $this->answer_model->set_nis($this->nis)
            ->set_exam_code($data_soal->kode_soal)
            ->set_req_body(json_encode($output));
            $this->answer_model->write_on_remote();

            redirect('question/show/'.$data_soal->kode_soal);

      //    print_r($list_id_soal);
      //    print_r($output);
          }else{
            $err= new \stdClass;
            $err->type='warning';
            $err->msg='passcode salah';
            $this->template->load('layout/main','errors/html/human_error',array('error'=>$err));

          }
      }else{
        //not this time
        $this->template->load('layout/main','exam/not_this_time');
      }

    }else{
      echo "nothing to do";
    }
  }

  private function validate_exam(){
    return true;
  }

  private function ordering_number($data, $acak = false){

  }

  private function start_the_exam($id_soal,$duration){

    $this->load->library('numbertime');
    //set time user start and must finish the exam
    $this->data_nilai_model
    ->set_id_soal_uji($id_soal)
    ->set_nis($this->nis)
    ->set_benar(0)
    ->set_salah(0)
    ->set_nilai(0)
    ->set_mulai($this->numbertime->timestamp_to_time(time()))
    ->set_selesai($this->numbertime->timestamp_to_time(time()+($duration*60)))
    ->set_pelanggaran(0)
    ->create();

  }


  public function get_answer($exam_code){
    $this->answer_model
    ->set_nis($this->nis)
    ->set_exam_code($exam_code);
    $output = $this->answer_model->read_on_remote();
    print_json($output);
  }







  public function update_answer($exam_code){
    $this->load->library('numbertime');
    $input_data = json_decode(file_get_contents('php://input'));

    //get detail pengerjaan ujian
    $detail = $this->data_nilai_model->show_by_nis_and_ujian($this->nis,$exam_code,array('data_nilai.mulai','data_nilai.selesai','jum_soal'));

    //find out when this things posted
    $cur_time = new DateTime($this->numbertime->timestamp_to_time(time()));
    $str_time = new DateTime($detail->mulai);
    $interval = date_diff($str_time,$cur_time);

    //interval in second
    $interval_tot = ($interval->h*60*60)+($interval->i*60)+($interval->s);

    //get posted data

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

      $answer_data=$input_data->answer_data;
      $pelanggaran=$input_data->foul;

      $input = array('time'=>$interval_tot,'pelanggaran'=>$pelanggaran,'data'=>$answer_data);
      $this->answer_model->set_nis($this->nis)
      ->set_exam_code($exam_code)
      ->set_req_body(json_encode($input));
      $this->answer_model->rewrite_on_remote();
      }
  }


    public function result_list(){
      $lst_hasil = $this->data_nilai_model->list_by_nis($this->nis,
      array(
        'data_nilai.id','nis','data_nilai.selesai','nilai','kode_soal'
      ));
      foreach ($lst_hasil as $key => $value) {
        # code...
        $data_mapel = $this->mapel_model->trace_by_exam_code($value->kode_soal);
        $value->mapel = $data_mapel->nama;
      }
      //print_r($lst_hasil);
      $this->template->load('layout/main','exam/result_list',array('data_nilai'=>$lst_hasil));

    }
    public function result_detail($id){
      $detail = $this->data_nilai_model->get_by_id($id,$this->nis);
      $detail->soal_uji = $this->soal_uji_model->get_by_id($detail->id_soal_uji);
      $detail->guru = $this->sh_guru_staff_model->show($detail->soal_uji->nip);
      $detail->mapel = $this->mapel_model->trace_by_exam_code($detail->soal_uji->kode_soal);
      $detail->kd = $this->kd_model->get_by_exam_code($detail->soal_uji->kode_soal);
      $this->template->load('layout/main','exam/result_detail',array(
        'detail'=>$detail ));
//print_r($detail);
    }

    //user finish and submit exam
    public function finish($exam_code){
      $this->db->cache_delete('exam', 'result_detail');
      $detail = $this->data_nilai_model->show_by_nis_and_ujian($this->nis,$exam_code,
      array('data_nilai.id','nilai','data_nilai.mulai','data_nilai.selesai','jum_soal','submit','tampil_nilai'));
      $selesai = new DateTime($detail->selesai);
      $now = new DateTime($this->numbertime->timestamp_to_time(time()));

      if($now>$selesai){
        //timeout!!
        if($detail->submit < 1){
          //jika belum disubmit maka hitung (karena timeout, tak perlu mengupdate kolom selesai, cukup nilai benar salah submit aja)
          $detail->nilai = $this->penilaian($exam_code,true);
          //print_r($detail);
          //tampilkan halaman waktu habis
        }else{
          //sudah disubmit, jadi tak perlu di hitung lagi
          //tampil halaman selesai / rekap
        }
      }else{
        $detail->nilai = $this->penilaian($exam_code,false);
        //waktu sudah selesai, tapi sudah disubmit, langsung hitung nilai
        //tampil halaman selesai
      }

      $this->template->load('layout/main','exam/finish',array('detail'=>$detail));
    }

    private function penilaian($exam_code,$telat=false){
      $this->load->library('numbertime');
      $this->answer_model->set_nis($this->nis)
      ->set_exam_code($exam_code);
      $answer = $this->answer_model->read_on_remote();
      $detail_ujian = $this->soal_uji_model->detail($exam_code);
      $benar = 0;
      foreach ($answer->data as $key => $value) {
        # code...

        $soal = $this->butir_soal_model->show($value->id);
        if($value->ans == $soal->kunci){
          $benar =  $benar + 1;
        }

        if($soal->is_essay>0){
          $this->session->set_flashdata('is_essay',1);
        }


      }
      $nilai = ((1/$detail_ujian->jum_soal)*100)*$benar;
      $salah = $detail_ujian->jum_soal-$benar;
      $data_nilai = $this->data_nilai_model->show_by_nis_and_ujian($this->nis,$exam_code);
      $selesai = (($telat==true)?$data_nilai->selesai:$this->numbertime->timestamp_to_time(time()));
  		$this->data_nilai_model
      ->set_id($data_nilai->id)
  		->set_nis($data_nilai->nis)
  		->set_id_soal_uji($data_nilai->id_soal_uji)
  		->set_benar($benar)
  		->set_salah($salah)
  		->set_nilai($nilai)
  		->set_mulai($data_nilai->mulai)
  		->set_selesai($selesai)
      ->set_ip($data_nilai->ip)
  		->set_pelanggaran($answer->pelanggaran)
      ->set_submit("1")
      ->update();
      return $nilai;

    }

}
