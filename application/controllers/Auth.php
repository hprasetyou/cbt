<?php

class Auth extends CI_Controller{
  public function __construct()
      {
        parent::__construct();
        $this->load->model('siswa_20162017_model');

      }

  public function login(){
    if($this->session->userdata('logged_in')){
      redirect('exam');
    }else{

      $this->template->load('layout/blank','auth/login');
    }

  }

  public function do_login(){
    $this->load->library('numbertime');
    $input_data = json_decode(file_get_contents('php://input'));
    $output=array();

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // …
        //attempt count
        $count= ($this->session->tempdata('count')?$this->session->tempdata('count'):0);
        $last_attempt = $this->session->tempdata('last_attempt');

        if($count<3){
            $output = array();
            //get post data
            $nis = $input_data->nis;
            $pwd =$input_data->password;
            //find user with that posted nis
            $data_siswa = $this->siswa_20162017_model->show($nis);
            if(count($data_siswa)>0){
              //if exist
              if(password_verify($pwd,$data_siswa->password)){
                //if password match, login success, set session data
                $newdata = array(
                  'user_id'   => $data_siswa->nis,
                  'nama'      => $data_siswa->nama,
                  'logged_in_at'=> $this->numbertime->timestamp_to_time(time(),'d-M-Y H:i:s'),
                  'kode_kelas'=> $data_siswa->kode_kelas,
                  'logged_in' => TRUE
                  );

                $this->session->set_userdata($newdata);
                $output = array('status'=>'ok','message'=>'user_authenticated');
              }
              else{
                $this->session->set_tempdata('count', $count+1 ,10);
                //if password didn't match
                $output = array('status'=>'error','message'=>'password missmatch','attempt'=>$count);
              }

            }else{
              $this->session->set_tempdata('count', $count+1 ,10);
              //user not exist
              $output = array('status'=>'error','message'=>'user not found','attempt'=>$count);
            }
        }else{
          $this->session->set_tempdata('count', $count+1 ,10);
          $output = array('status'=>'error','message'=>'wait 10 second');
        }


    }
    print_json($output);
  }

  public function logout(){
      //clear all cache
      $this->db->cache_delete_all();
    $user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
    $this->session->sess_destroy();
    redirect('login');
  }
}
