<?php
class Profile extends CI_Controller{
  public function __construct()
      {
        parent::__construct();
        $this->load->model('siswa_20162017_model');
        $userdata = auth_data();
        $this->nis = $userdata->user_id;
      }
  //assign with session_id
  private $nis;

  public function index(){
    $id = $this->nis ;//change with user_session later
    $siswa = $this->siswa_20162017_model->show($id);
    if(count($siswa)>0){
      $this->template->load('layout/main','profile/index',
      array('userdata'=>$siswa));
    }else{
      show_404();
    }
  }

  public function do_upload()
        {
        $siswa = $this->siswa_20162017_model->show($this->nis);
                $config['upload_path']          = './upload/img';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 500;
                $config['max_width']            = 1024;
                $config['max_height']           = 1024;
                $config['encrypt_name']         = TRUE;
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                      $this->session->set_flashdata('msg',$this->upload->display_errors());
                      redirect('profile');
                }
                else
                {
                        $upload_data = $this->upload->data();
                        $file_name = $upload_data['file_name'];
                        $this->siswa_20162017_model->set_img_url(base_url().'/upload/img/'.$file_name)->update();
                        $this->session->set_flashdata('msg','file uploaded');
                        redirect('profile');
                }
        }
}
