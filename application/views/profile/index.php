<div class="container">
  <?php
  if($this->session->flashdata('msg')){
    echo '<div class="alert alert-info"><span class=" glyphicon glyphicon-info-sign"></span>'.$this->session->flashdata('msg').'</div>
';
  }
   ?>
  <div class="panel panel-defaul">
    <div class="panel-body">
  <div class="row">
    <div class="col-md-12 col-lg-12">
      <div class="container">
        <h3>Profil Siswa</h3>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-md-3 col-xs-6 col-lg-3">
      <a href="#" class="thumbnail" data-toggle="modal" data-target="#myModal">
        <img src="<?php echo $userdata->img_url ?>" onerror="this.src = '<?php echo base_url()?>asset/images/index.svg';" />
      </a>
    </div>
    <div class="col-md-9 col-lg-9" style="font-size:17px">

      <div class="row">
        <div class="col-md-3">Nomor Induk Siswa</div>
        <div class="col-md-1">:</div>
        <div class="col-md-8"><?php echo $userdata->nis ?></div>
      </div>
      <div class="row">
        <div class="col-md-3">Nama Siswa</div>
        <div class="col-md-1">:</div>
        <div class="col-md-8"><?php echo $userdata->nama ?></div>
      </div>

    </div>
  </div>

</div>
</div>
</div>




<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ubah foto profil</h4>
      </div>
        <?php echo form_open_multipart('profile/do_upload');?>
      <div class="modal-body">

          <label for="userfile">Upload foto</label><input type="file" name="userfile" id="userfile" size="20" />

          <br /><br />



      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="upload" />
      </div>
        </form>
    </div>

  </div>
</div>
