
<?php
foreach ($alert as $key => $value) {
  # code...
  echo '<div class="alert alert-'.$value->type.'">
    <p>
      <span class="glyphicon glyphicon-info-sign info"></span>
      '.$value->msg.'
    </p>
  </div>';
}

 ?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h5>Detail Ujian</h5></div>
  <div class="panel-body">




    <div class="">

    </div>
    <div class="row">
      <div class="col-md-3 col-sm-10">Kode Ujian</div>
      <div class="col-md-1 col-sm-2">:</div>
      <div class="col-md-7"><?php echo $detail->kode_soal ?></div>
    </div>
    <div class="row">
      <div class="col-md-3 col-sm-10">Nama guru</div>
      <div class="col-md-1 col-sm-2">:</div>
      <div class="col-md-7"><?php echo $detail->guru->nama_gurustaff ?></div>
    </div>
    <div class="row">
      <div class="col-md-3 col-sm-10">Mapel</div>
      <div class="col-md-1 col-sm-2">:</div>
      <div class="col-md-7"><?php echo $detail->mapel->nama ?></div>
    </div>
    <div class="row">
      <div class="col-md-3 col-sm-10">Kd</div>
      <div class="col-md-1 col-sm-2">:</div>
      <div class="col-md-7"><?php foreach ($detail->kd as $key => $value) {
        # code...
          echo $value->nama.'<br>';
        }
        ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 col-sm-10">Jumlah soal</div>
      <div class="col-md-1 col-sm-2">:</div>
      <div class="col-md-7"><?php echo $detail->jum_soal ?> soal</div>
    </div>
    <div class="row">
      <div class="col-md-3 col-sm-10">Waktu Mulai Ujian</div>
      <div class="col-md-1 col-sm-2">:</div>
      <div class="col-md-7"><?php echo $detail->mulai ?></div>
    </div>
    <div class="row">
      <div class="col-md-3 col-sm-10">Waktu Ujian Berakhir</div>
      <div class="col-md-1 col-sm-2">:</div>
      <div class="col-md-7"><?php echo $detail->selesai ?></div>
    </div>
    <div class="row">
      <div class="col-md-3 col-sm-10">Keterangan</div>
      <div class="col-md-1 col-sm-2">:</div>
      <div class="col-md-7"><?php echo $detail->ket ?></div>
    </div>

  </div>
  <?php
      if(($status_pengerjaan['dikerjakan']==0)&&($status['status']=='info')){
        echo '<div class="panel-footer" >
          <div class="clearfix">
            <a href="#"  data-toggle="modal" data-target="#soalModal" class="btn btn-primary pull-right">Mulai Mengerjakan</a>
          </div>

        </div>';
      }
   ?>

</div>






<!-- Modal -->
<div class="modal fade" id="soalModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <?php
    $hidden = array('exam_code'=>$detail->kode_soal);
    echo form_open('exam/take','',$hidden); ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Mulai ujian</h4>
      </div>
      <div class="modal-body">
        <p><span class="glyphicon glyphicon-info-sign info"></span> Masukkan kode ujian untuk <?php echo ($status_pengerjaan['dikerjakan']==0) ? 'mulai mengerjakan':'melanjutkan' ?> ujian</p>
        <label for="exam_password">Password ujian</label>
        <input type="text" class="form-control" name="exam_password" id="exam_password" autofocus="autofocus">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <input type="submit" value="Kerjakan" name="btn" class="btn btn-primary">
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
<script type="text/javascript">

  localStorage.clear();
</script>
