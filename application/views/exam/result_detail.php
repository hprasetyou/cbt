<div class="panel panel-default">
  <div class="panel-heading">Detail</div>
  <div class="panel-body">
    
    <div class="">
      <div class="row">
        <div class="col-md-4">Kode Ujian</div>
        <div class="col-md-8"><?php echo $detail->soal_uji->kode_soal ?></div>
      </div>
      <div class="row">
        <div class="col-md-4">Mapel</div>
        <div class="col-md-8"><?php echo $detail->mapel->nama ?></div>
      </div>
      <div class="row">
        <div class="col-md-4">Kompetensi dasar</div>
        <div class="col-md-8"><?php foreach ($detail->kd as $key => $value) {
          # code...
          echo $value->nama.'';
        } ?></div>
      </div>
      <div class="row">
        <div class="col-md-4">Guru</div>
        <div class="col-md-8"><?php echo $detail->guru->nama_gurustaff ?></div>
      </div>
      <div class="row">
        <div class="col-md-4">Mulai</div>
        <div class="col-md-8"><?php echo $detail->mulai ?></div>
      </div>
      <div class="row">
        <div class="col-md-4">Selesai</div>
        <div class="col-md-8"><?php echo $detail->selesai ?></div>
      </div>
      <?php if($detail->soal_uji->tampil_nilai==1){
        echo '<div class="row">
          <div class="col-md-4">Nilai</div>
          <div class="col-md-8">'.$detail->nilai.'</div>
        </div>';
      }
      ?>

    </div>
  </div>
  <div class="panel-footer">
    <div class="clearfix">
      <?php echo anchor('exam/result_list','Kembali ke daftar ujian',array('class'=>'btn btn-default pull-right')) ?>
    </div>
    </div>
</div>
