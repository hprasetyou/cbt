<div class="panel panel-default">
  <div class="panel-heading">
    Ujian selesai
  </div>
  <div class="panel-body">
      <div class="alert alert-info">
        <p>
          <span class="glyphicon glyphicon-info-sign"></span>  Anda telah menyelesaikan ujian ini, klik tombol detail untuk melihat detail pengerjaan ujian
        </p>
      </div>
      <?php
        if($this->session->flashdata('is_essay')=='1'){
          echo '
          <div class="alert alert-info">
            <p>
              <span class="glyphicon glyphicon-info-sign"></span>
              Di dalam ujian ini terdapat soal esssay yang harus dinilai manual oleh guru
            </p>
          </div>';
        }
      ?>
      <br>
      <?php
        if($detail->tampil_nilai==1){
          echo '
          <center>
            <h5>Nilai anda</h5>
            <h3>'.$detail->nilai.'</h3>
          </center>
          ';
        }
      ?>


    <br>
    <br>
  </div>
  <div class="panel-footer"><?php echo anchor('exam/result_detail/'.$detail->id,'Detail',array('class'=>'btn btn-primary pull-right')) ?><br><br></div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  localStorage.clear();
}
</script>
