<div class="panel panel-default">
  <div class="panel-body">
    <h2>Daftar Ujian</h2>
    <table class="table table-hovered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Mapel</th>
          <th>Nama Guru</th>
          <th>Mulai</th>
          <th>Selesai</th>
          <th>Waktu</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i=0;
          foreach ($exam_list as $key => $value) {
            # code...
            $i++;
            echo '
            <tr>
              <td>'.$i.'</td>
              <td>'.$value->mapel.'</td>
              <td>'.$value->guru->nama_gurustaff.'</td>
              <td>'.$value->mulai.'</td>
              <td>'.$value->selesai.'</td>
              <td>'.$value->lama.' menit</td>
              <td>'.anchor('exam/show/'.$value->kode_soal,' <span class=" glyphicon glyphicon-search"></span> Detail',array('class'=>'btn btn-sm btn-default')).'</td>
            </tr>
            ';
          }
          if($i==0){
            echo '
   <tr>
     <td colspan="7"><center>Data kosong</center></td>
   </tr>';
          }
         ?>

      </tbody>
    </table>
  </div>
</div>
