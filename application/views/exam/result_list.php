<div class="panel panel-default">
  <div class="panel-body">
    <h2>Daftar ujian yang telah diselesaikan</h2>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Kode ujian</th>
          <th>Mapel</th>
          <th>Diselesaikan pada</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 0;
        foreach ($data_nilai as $key => $value) {
          # code...
          $i++;
          echo '
          <tr>
            <td>'.$i.'</td>
            <td> '.$value->kode_soal.'</td>
            <td>'.$value->mapel.'</td>
            <td>'.$value->selesai.'</td>
            <td>'.anchor('exam/result_detail/'.$value->id,' <span class=" glyphicon glyphicon-search"></span> Detail',array('class'=>'btn btn-sm btn-default')).'</td>
          </tr>';
        }
        ?>
      </tbody>
    </table>

  </div>
</div>
