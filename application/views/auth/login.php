<style>

.form-signin {
  margin-top: 100px;
  max-width: 330px;
  padding: 90px 15px 15px 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="Nomor Induk Siswa"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

.btn{
  padding: 10px 0 10px 0;
}

#message {
    z-index: 100;
    position: fixed;
    width: 80%;
}
#inner-message {
    margin: 0 auto;
}
.background-image {
  position: fixed;
  left: 0;
  right: 0;
  z-index: 1;

  display: block;
  background-image: url("<?php echo base_url().'asset/images/login-bg.png' ?>");
  /* Set rules to fill background */

    position: fixed;
    top: 0;
    left: 0;

    /* Preserve aspet ratio */
    min-width: 100%;
    min-height: 100%;
    background-repeat: no-repeat;
    background-size:cover

  /* Set up positioning */
  position: fixed;
  top: 0;
  left: 0;
  -webkit-filter: blur(7px);
  -moz-filter: blur(7px);
  -o-filter: blur(7px);
  -ms-filter: blur(7px);
  filter: blur(7px);
}

.content {
  position: fixed;
  left: 0;
  right: 0;
  z-index: 9999;
}
</style>


<center id="message">
  <div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <p id="message-body"></p>
  </div>
</center>
<div class="background-image"></div>
<div class="col-md-12 content" >
  <div class="col-md-6"></div>
  <div class="col-md-6">
    <div class="col-md-2"></div>
    <div class="col-md-10">
      <form class="form-signin">
              <h2 class="form-signin-heading">Silahkan login</h2>
              <p>
                Login menggunakan akun anda
              </p>
              <label for="inputNis" class="sr-only">Nomor Induk Siswa </label>
              <input type="text" id="inputNis" class="form-control" placeholder="Nomor Induk Siswa " required autofocus>
              <label for="inputPassword" class="sr-only">Password</label>
              <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="remember-me"> Remember me
                </label>
              </div>
              <a href="#" id="btn-login" class="btn btn-lg btn-primary btn-block" >Login</a>
              <br>
              <a href="#">Tidak bisa login?</a>
            </form>
    </div>
  </div>
</div>

<script type="text/javascript">
$('#message').hide();
$('#btn-login').click(function(){

  $.ajax
   ({
       type: "POST",
       //the url where you want to sent the userName and password to
       url: '<?php echo site_url('auth/do_login') ?>',
       dataType: 'json',
       async: false,
       //json object to sent to the authentication url
       data: JSON.stringify({ "nis": $('#inputNis').val(), "password" : $('#inputPassword').val() }),
       success: function (msg) {
         console.log(msg);
         if(msg.status=='ok'){
           window.location = '<?php echo site_url('exam') ?>'
         }else{
           $('#message').show();
           $('#message-body').html(msg.message);
           setInterval(function(){
             $('#message').fadeOut("slow");
           },1000)

         }
       }
   })

})
</script>
