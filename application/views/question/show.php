
  <div class="col-md-9">
    <h2>SOAL <?php echo $exam_code ?></h2>
    <i>
    <span id="kd" class="pull-right"></span></i>
  </div>
  <div class="col-md-3" >


  </div>

<div class="col-md-9">
  <div class="panel panel-primary">
    <div class="panel-heading">

        <span id="question_title">SOAL NO 1</span>

    </div>
    <div class="panel-body noselect">

      <div class="question">
      </div>
      <form>
        <div class="answer">
        </div>

      </form>
      <a href="#" id="prev" class="btn btn-default">Kembali</a>

      <a href="#" id="hesitate" class="btn btn-warning" >Ragu-ragu</a>

      <a href="#" id="next" class="btn btn-success">Selanjutnya</a>
    </div>
  </div>
</div>
<div class="col-md-3">
  <div class="panel panel-primary" >
    <div class="panel-heading">Info soal</div>
    <table class="table table-striped">
      <tr >
        <td>Nama Mapel</td>
        <td>:</td>
        <td id="mapel">P.A.I</td>
      </tr>
      <tr>
        <td>Nama Guru</td>
        <td>:</td>
        <td><?php echo $detail->guru->nama_gurustaff ?></td>
      </tr>
    </table>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading">
      Navigasi soal
    </div>
    <div class="panel-body" style="padding-right:0px">
      <?php
        for($i=1;$i<=$detail->jum_soal;$i++){
          echo '
          <a href="#" onclick="navigate_to('.$i.')">
          <div id="no-'.$i.'" class="col-sm-2 unanswered" style="margin:2px 2px 2px 2px;padding:1px 1px 0 0" >
          <div style="width:50%;height:50%;background:#000"></div>
          <div class="col-sm-8" style="padding-left: 3px; padding-right: 3px;">'.$i.'</div><div id="jwb-'.$i.'" class="col-sm-4" style=" padding-left: 3px; padding-right: 3px;font-size:10px"></div>
          </div>
          </a>';
        }
      ?>
    </div>
    <div class="panel-footer"><a href="#" data-toggle="modal" data-target="#selesaiModal" class="btn btn-success">Selesai</a></div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="selesaiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-info-sign info"></span>Konfirmasi?</h4>
      </div>
      <div class="modal-body">
        <p> Apakah anda yakin?</p>
        <ul>
          <li>Waktu masih tersisa <span class="timer"></span></li>
          <li>Jumlah soal yang sudah dikerjakan : <span class="soal_selesai"></span></li>
          <li>Jumlah soal yang belum dikerjakan : <span class="soal_belum"></span></li>
          <li>Jumlah soal yang ditandai masih ragu : <span class="soal_ragu"></span></li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button id="yakin" type="button" class="btn btn-default">Ya</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="Modalcurang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-warning-sign"></span>Peringatan</h4>
      </div>
      <div class="modal-body">
        <p> Keluar dari jendela ini akan dianggap sebagai kecurangan</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Saya mengerti</button>
      </div>
    </div>
  </div>
</div>
<script>
var num = 1;
var foul = 0;
$(document).ready(function() {
  $('#timebar').show();
  get_ans_list();
  navigate_to(1);

  $('#sidebar-wrapper').hide();
  $('#wrapper').css('padding-left','0px');
  $('#wrapper.active').css('padding-left','0px');
  $('.content').css('padding','0 0 0 0');
  $('#page-content-wrapper').css('padding-left: 0px');

});
document.addEventListener('contextmenu', event => event.preventDefault());
function get_ans_list(){
  var url = "<?php echo site_url('exam/get_answer/'.$exam_code) ?>";
    $.getJSON(url, function(result){
        localStorage.setItem('ans_list', JSON.stringify(result.data));
        foul = result.pelanggaran;
        var jwb = result.data;
        for(a in jwb){
          set_answer_status(jwb[a].no,jwb[a].ans,jwb[a].opsi)
        }
    });
}

var ans_tot=0;

function get_ans(number){
  var ans_list = JSON.parse(localStorage.getItem('ans_list'));
  for(var i in ans_list){
    set_answer_status(ans_list[i].no,ans_list[i].ans,ans_list[i].opsi)
    if(ans_list[i].no==number){
      return ans_list[i];
    }
  }
}

function ans_by_num(number){
  var ans_list = JSON.parse(localStorage.getItem('ans_list'));
  for(var i in ans_list){
    if(ans_list[i].no==number){
      return ans_list[i];
    }
  }
}


var a= 0;
setInterval(function() {
  // method to be executed;
send_answer();
}, 1000*60*5);


function send_answer(){
  var ans_list = JSON.parse(localStorage.getItem('ans_list'));
  $.ajax
   ({
       type: "POST",
       //the url where you want to sent the userName and password to
       url: '<?php echo site_url('exam/update_answer/'.$exam_code) ?>',
       dataType: 'json',
       async: false,
       //json object to sent to the authentication url
       data: JSON.stringify({ "answer_data": ans_list , "foul":foul }),
       success: function (msg) {
       }
   })
}

function answer(ans,i){

  var ans_list = JSON.parse(localStorage.getItem('ans_list'));
  ans_list[i].ans = ans;
  localStorage.setItem('ans_list', JSON.stringify(ans_list));
  get_ans(ans_list[i].no)
}



$('#hesitate').click(function(){
  h_list= [];
  if(localStorage.getItem('h_list')!= null){
    h_list = JSON.parse(localStorage.getItem('h_list'));
  }
    if(!h_list.includes(num)){
      $('#no-'+num).addClass('hesitate')
      h_list.push(num);
      $('#hesitate').addClass('active');
      localStorage.setItem('h_list', JSON.stringify(h_list));
    }
    else
    {
      var idx = h_list.indexOf(num);

      h_list.splice(idx,1);

      $('#no-'+num).removeClass('hesitate')
      localStorage.setItem('h_list',JSON.stringify(h_list));
      $('#hesitate').removeClass('active');
    }

    $('.soal_ragu').html(h_list.length+" soal");

})



$(".timer")
  .countdown("<?php echo $detail->selesai?>", function(event) {
    $(this).text(
      event.strftime('%H jam : %M menit : %S detik')
    );

    var tot_min = event.strftime('%M')*1;
    var tot_jam = event.strftime('%H')*1;
    if(tot_jam < (1)){
      if(tot_min < (1)){
        $('#timebar').removeClass('alert-warning');
        $('#timebar').removeClass('alert-success');
        $('#timebar').addClass('alert-danger');
      }else if (tot_min < (5)) {

          $('#timebar').removeClass('alert-success');
          $('#timebar').addClass('alert-warning');
      }else{}
    }

  }).on('finish.countdown', function(){
      finish_exam();
  });

$('#next').click(function(){
  num= (num>=<?php echo $detail->jum_soal ?>)?num:num+1;
  navigate_to(num);
})
$('#prev').click(function(){
  num= (num>1)?num-1:1;
  navigate_to(num);
})

function navigate_to(slc_num){
    num = slc_num;
    look_for_question(num);

    $('#question_title').html('SOAL NO '+num);
}

function store_questions(data){
  localStorage.setItem('question', JSON.stringify(data));
}

function add_to_question_list(new_data){
  if (localStorage.getItem('question') != null) {
    //...
    var q_list = JSON.parse(localStorage.getItem('question'));

    for(var i in new_data){
      q_list.push(new_data[i]);
    }
    store_questions(q_list);
  }
  else{
    store_questions(new_data);
  }
}

function get_question(num){
  loading(true);
  var url = "<?php echo site_url('question/take/'.$exam_code.'/') ?>"+(<?php echo $detail->jum_soal ?><num? num :num-1);
    $.getJSON(url, function(result){
        add_to_question_list(result);
        setTimeout(function(){
          loading(false);
          look_for_question(num);

        },800)
    });
}

function nextChar(c) {
    return String.fromCharCode(c.charCodeAt(0) + 1);
}

var lst_answered=[];

function set_answer_status(n,ans,order){
  h_list = [];
  if(localStorage.getItem('h_list')!= null){
    h_list = JSON.parse(localStorage.getItem('h_list'));
  }
  if(h_list.includes(n)){
      $('#no-'+n).addClass('hesitate')

        if(!lst_answered.includes(n) && ans!=""){
          lst_answered.push(n);
        }
  }else{
    if(ans==""){
    }else{
      if(!lst_answered.includes(n)){
        lst_answered.push(n);
      }
      $('#no-'+n).removeClass('unanswered')
      $('#no-'+n).addClass('answered')
    }
  }
  var opt = order.split("");
  var d_ans="";
  var i_char = 'a';
  for(var o in opt){
    if(opt[o]==ans){
      d_ans = i_char;

    }else{
      i_char = nextChar(i_char);
    }
  }
  $('.soal_selesai').html(lst_answered.length+" soal");
  $('.soal_ragu').html(h_list.length+" soal");
  $('.soal_belum').html(<?php echo $detail->jum_soal ?>-lst_answered.length+" soal")
  $('#jwb-'+n).html('<b>'+d_ans.toUpperCase()+'</b>');
}



function look_for_question(num){
  var found = false;
  var q = JSON.parse(localStorage.getItem("question"));
  for (var i in q){
console.log(q[i].no);
    if(q[i].no==num){
      var u_answer = get_ans(q[i].no);

      found=true;
      var datasoal = q[i].soal;
      if(q[i].sound == null || q[i].sound==""){

      }
      else{
        datasoal += '<audio loop="loop" autoplay="autoplay">'+
        '<source src="'+ q[i].sound +'" type="audio/mpeg" />'+
        '</audio>';
      }

      $('.question').html(datasoal);
      $('#mapel').html(q[i].mapel);
      $('#kd').html(q[i].kd)
      var opt_order = q[i].opt_order;
      var opt = opt_order.split("");
      var opt_list='';
      var opt_index='';
      var i_char = 'a';
      for(var o in opt){
        var option="";
        switch(opt[o]){
          case 'a':
            option = q[i].opsi_a;
            opt_index = 'a';
            break;
          case 'b':
            option =  q[i].opsi_b;
            opt_index = 'b';
            break;
          case 'c':
            option =  q[i].opsi_c;
            opt_index = 'c';
            break;
          case 'd':
            option =  q[i].opsi_d;
            opt_index = 'd';
            break;
          case 'e':
            option =  q[i].opsi_e;
            opt_index = 'e';
            break;

        }



        if(localStorage.getItem('h_list')!= null){
        var h_list = JSON.parse(localStorage.getItem('h_list'));
        if(h_list.includes(num)){
          $('#hesitate').addClass('active');
        }else{
            $('#hesitate').removeClass('active');

          }
        }

        opt_list += '<div class="radio" onclick="answer('+"'"+opt_index+"'"+','+i+')">'+
        '<label><input type="radio" '+((opt_index==u_answer.ans?'checked="checked"':''))+
        ' name="opt" value="'+opt_index+'"> '+i_char+'. '+option+'</label>'+
        '</div>';
        i_char = nextChar(i_char);

      }
      if(q[i].is_essay>0){
        $(".answer").html('<br><label for="ess_ans">Tulis jawaban anda disini</label><br><textarea id="ess_ans" name="ess_ans" onblur="save_ess('+i+')" class="form-control">'+u_answer.ans+'</textarea> <br><br><br>');
      }else{
        $(".answer").html(opt_list);
      }

    }
  }
  if(!found){
    get_question(num);
  }
}
var submited = false;

function save_ess(x){
  var answer_ess = $('#ess_ans').val();
  var ans_list = JSON.parse(localStorage.getItem('ans_list'));
  ans_list[x].ans = answer_ess;
  localStorage.setItem('ans_list', JSON.stringify(ans_list));
  get_ans(ans_list[x].no)
}



$('#yakin').click(function(){
  send_answer();
finish_exam();
})

function finish_exam(){
  loading(true);
  submited = true;
  localStorage.clear();
  setTimeout(function () {
    loading(false);
      window.location = '<?php echo site_url('exam/finish/'.$exam_code) ?>'
  }, 1000);
}

window.onbeforeunload = function(e) {
  if(!submited){
    var dialogText = 'Dialog text here';
    send_answer();
    e.returnValue = dialogText;
    return dialogText;

  }
};

var out = false;

$(document).mouseleave(function () {
  // do work
  out = false;

  $('#Modalcurang').modal('show')
  setTimeout(function(){
    out = true;
  },8000);
});

$(document).mouseenter(function () {
  // do work
  if(out){
    foul = foul+1;

  }


});
</script>
