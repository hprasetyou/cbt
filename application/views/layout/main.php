<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Aplikasi CBT</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>asset/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>asset/theme/flatly/theme.min.css" rel="stylesheet">

    <script src="<?php echo base_url() ?>bower_components/jquery/dist/jquery.min.js" ></script>

    <script src="<?php echo base_url() ?>bower_components/jquery.countdown/dist/jquery.countdown.min.js" ></script>
    <style media="screen">

    .navbar-brand {
      padding: 0px;
    }
    .navbar-brand>img {
      height: 100%;
      padding: 5px;
      width: auto;
    }

    html {
    position: relative;
    min-height: 100%;

    }
    .content{
      margin-bottom: 120px;
    }

    footer {
        position: absolute;
        left: 0;
        bottom: 0;
        width: 100%;
        overflow:hidden;
        padding: 25px;
    }
    footer > .container{
      width: 100%;
    }
    </style>
  </head>

  <body>

  <div id="wrapper" class="active" >

        <!-- Sidebar -->
              <!-- Sidebar -->
        <div id="sidebar-wrapper">
        <ul id="sidebar_menu" class="sidebar-nav">
             <li class="sidebar-brand"><a id="menu-toggle" href="#">Menu<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>
        </ul>
          <ul class="sidebar-nav" id="sidebar">
            <li><?php echo anchor('exam','Daftar Ujian <span class="sub_icon glyphicon glyphicon-file"></span>') ?></li>
            <li><?php echo anchor('exam/result_list','Hasil Ujian <span class="sub_icon glyphicon glyphicon-list-alt"></span>')?></li>
            <li class="active"><?php echo anchor('profile','Profil<span class="sub_icon glyphicon glyphicon-user"></span>') ?></li>
          </ul>
        </div>

        <!-- Page content -->
        <div id="page-content-wrapper">
          <!-- Static navbar -->
          <div style="margin-bottom:0px" class="navbar navbar-default" role="navigation">
       <div class="container">
           <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
               </button>
               <a target="_blank" href="#" class="navbar-brand"> <img src="<?php echo base_url() ?>/asset/images/tutwuri.png"  alt="Aplikasi CBT"> </a>
           </div>
           <div class="collapse navbar-collapse">

               <ul class="nav navbar-nav navbar-right ">
                   <li class="dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                           <span class="glyphicon glyphicon-user"></span> 
                           Anda login sebagai <strong><?php echo $this->session->userdata('nama') ?></strong>
                           <span class="glyphicon glyphicon-chevron-down"></span>
                       </a>
                       <ul class="dropdown-menu">
                           <li>
                               <div class="navbar-login">
                                   <div class="row">
                                       <div class="col-lg-4">
                                           <p class="text-center">
                                               <span class="glyphicon glyphicon-user icon-size"></span>
                                           </p>
                                       </div>
                                       <div class="col-lg-8">
                                           <h5>NIS: <?php echo $this->session->userdata('user_id') ?></h5>
                                           <h5>Selamat datang, <?php echo $this->session->userdata('nama') ?></h5>
                                           <p style="margin-top: 10px">
                                               <?php echo anchor('auth/logout','>>Logout');?>
                                           </p>
                                       </div>
                                   </div>
                               </div>
                           </li>
                       </ul>
                   </li>
               </ul>
           </div>
       </div>
      </div>
      <div id="timebar" style="display:none" class="col-md-12 alert alert-success">
          <center class="timer" style="font-size:18px">
              00:00:00
          </center>
      </div>
      <div class="container content" >
          <div class="overlay">
              <div id="loading-img"></div>
          </div>
          <?php echo $contents ?>
      </div> <!-- /container -->
    </div>
    <footer class="footer navbar-default">
        <div class="container">
          <p class="text-muted pull-right">Alamat ip:<?php echo getrealip() ?> | Login pada <?php echo $this->session->userdata('logged_in_at') ?>| Status <span class="connstatus">Connected</span></p>
        </div>
    </footer>
  </div>


<script type="text/javascript">
$(document).ready(function(){
})
  function loading(isloading){
    if(isloading){
      $('.overlay').show();

    }else{
    setTimeout(function(){
      $('.overlay').fadeOut("fast");
    },1000);

    }
  }
  $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
  });



setInterval(function(){
  $.get("<?php echo site_url('welcome') ?>", function(){
    //success
    $('.connstatus').html('Connected');
    console.log('connected');
  }).fail(function(){
    //fail
    $('.connstatus').html('Disconnected');
    console.log('disconnected');
  })
},300000);



  </script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url() ?>bower_components/bootstrap/dist/js/bootstrap.min.js" ></script>
  </body>
</html>
