<?php
if(!function_exists('print_json'))
{
    function print_json($data/*array*/)
    {

        header('Content-Type: application/json');
        echo json_encode($data);
    }
}
