<?php
if(!function_exists('auth_data'))
{
    function auth_data()
    {
    $CI =& get_instance();
    $is_logged_in = $CI->session->userdata('logged_in');
       if($is_logged_in)
       {
         $output = new stdClass;
         $output->user_id = $CI->session->userdata('user_id');
         $output->nama = $CI->session->userdata('nama');
         $output->kode_kelas = $CI->session->userdata('kode_kelas');
         return $output;
       }else{
          redirect('login');
       }
    }
}
